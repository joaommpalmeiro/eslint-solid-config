# Notes

- https://github.com/azat-io/eslint-config:
  - https://github.com/azat-io/eslint-config/tree/v1.7.1/typescript
  - https://github.com/azat-io/eslint-config/tree/v1.7.1/react
- https://eslint.nuxt.com/packages/config#flat-config-format
- https://github.com/nuxt/eslint/blob/9dfbc5228f52824edd28bba997f35ff1140d3b83/packages/eslint-config/package.json
- Vite:
  - https://vitejs.dev/guide/build#library-mode
  - https://vitejs.dev/config/build-options#build-lib
- https://github.com/sindresorhus/array-equal/blob/dc9ae88e1b06d3ba8823f90f82b63e4d19c8de3d/package.json:
  - `"type": "module"`
  - `"exports": { "types": "./index.d.ts", "default": "./index.js" }`
  - `"engines": { "node": ">=18" }`
- https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c
- https://dev.to/receter/how-to-create-a-react-component-library-using-vites-library-mode-4lma
- https://andrewwalpole.com/blog/use-vite-for-javascript-libraries/
- https://rbardini.com/how-to-build-ts-library-with-vite/:
  "(...) use Vite's library mode, and externalize all dependencies and built-in Node.js modules so that they are not bundled with your code (...)"
- https://github.com/weiran-zsd/dts-cli
- https://v8.dev/features/import-assertions: `import json from './foo.json' assert { type: 'json' };`
- https://github.com/azat-io/eslint-config/blob/v1.7.1/tsconfig.json

## Snippets

### `vite.config.js` by [Rafael Bardini](https://rbardini.com/)

```ts
import { defineConfig } from "vite";
import dts from "vite-plugin-dts";
import pkg from "./package.json" assert { type: "json" };

export default defineConfig({
  build: {
    lib: {
      entry: "./src/index.ts",
      formats: ["es"],
    },
    rollupOptions: {
      external: [...Object.keys(pkg.dependencies), /^node:.*/],
    },
    target: "esnext",
  },
  plugins: [dts()],
});
```

### `vite.config.ts` by [Azat S.](https://github.com/azat-io)

```ts
import { defineConfig } from "vite";
import dts from "vite-plugin-dts";
import path from "node:path";

export default defineConfig({
  build: {
    lib: {
      fileName: (format) => `index.${format === "es" ? "mjs" : "js"}`,
      entry: path.join(__dirname, "index.ts"),
      formats: ["cjs", "es"],
    },
    rollupOptions: {
      external: (id: string) =>
        !id.startsWith(".") &&
        !path.isAbsolute(id) &&
        id !== "eslint-define-config",
    },
  },
  plugins: [
    dts({
      outDir: path.join(__dirname, "dist"),
      root: path.join(__dirname, ".."),
      entryRoot: __dirname,
      include: [
        path.join(__dirname, "..", "env.d.ts"),
        path.join(__dirname, "index.ts"),
      ],
    }),
  ],
});
```
