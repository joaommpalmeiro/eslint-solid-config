# `@joaopalmeiro/eslint-solid-config`

My personal ESLint config for [Solid](https://docs.solidjs.com/)/[SolidStart](https://docs.solidjs.com/solid-start) projects.
